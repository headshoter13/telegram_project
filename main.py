from telegram.ext import Updater, CommandHandler, \
    MessageHandler, ConversationHandler, Filters
from functions.telegram_func import TelegramFunc
import config
import logging

# This section is only to disable extra warnings due to proxy. Will be removed before deploy
from telegram.vendor.ptb_urllib3 import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


def main():
    # Init params
    choice = range(1)
    connect_conf = config.init_config()
    t_functions = TelegramFunc(choice)

    # Run Bot
    updater = Updater(token=connect_conf['token'], use_context=True, request_kwargs=connect_conf['connect'])
    dispatcher = updater.dispatcher

    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', t_functions.start)],
        states={
            choice: [
                MessageHandler(Filters.regex('Как купить?'), t_functions.buy_info),
                MessageHandler(Filters.regex('^💥Функции Instagram💥$'), t_functions.insta_tools),
                MessageHandler(Filters.regex('Мой баланс'), t_functions.check_the_balance),
                MessageHandler(Filters.photo, t_functions.send_payment_details)
            ]
        },
        fallbacks=[CommandHandler('start', t_functions.start)]
    )

    dispatcher.add_handler(conv_handler)
    dispatcher.add_error_handler(t_functions.error)

    updater.start_polling()
    logging.info("Bot is started")


if __name__ == '__main__':
    main()
