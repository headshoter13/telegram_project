import logging
from telegram import InlineKeyboardButton, InlineKeyboardMarkup,\
    ReplyKeyboardMarkup, KeyboardButton


class TelegramFunc(object):
    """
    TelegramFunc class:
        The class contains callable methods, which are called in main
    """

    def __init__(self, choice):
        self.choice = choice

    def start(self, update, context):
        user = update.message.from_user
        logging.info("User %s started the conversation.", user.username)
        context.bot.send_message(chat_id=update.message.chat_id,
                                 text=f"Привет, {user.username}! Я бот, который разведет тебя на бабки!")
        keyboard = [
            [KeyboardButton("💥Функции Instagram💥"),
             KeyboardButton("Как купить?💰"),
             KeyboardButton("💰Мой баланс💰")]
        ]

        update.message.reply_text(
            "Что тебе интересно?",
            reply_markup=ReplyKeyboardMarkup(keyboard, resize_keyboard=True, one_time_keyboard=False)
        )
        return self.choice

    @staticmethod
    def insta_tools(update, context):
        user = update.message.from_user
        logging.info("User %s chose section 'Функции Instagram'", user.username)

        keyboard = [
            [InlineKeyboardButton("Анализ аудитории", callback_data='analyze'),
             InlineKeyboardButton("Принять подписчиков", callback_data='add_all')]
        ]

        reply_markup = InlineKeyboardMarkup(keyboard, resize_keyboard=True)
        update.message.reply_text(
            "Выбери функцию:",
            reply_markup=reply_markup
        )

    @staticmethod
    def buy_info(update, context):
        user = update.message.from_user
        logging.info("User %s chose section 'Как купить?'", user.username)

        context.bot.send_message(chat_id=update.message.chat_id,
                                 text=f"{user.username},\nМы очень рады, что вас заинтересовал наш продукт!\n\n"
                                      f" Пополните баланс и получите доступ к нашим сервисам:\n\n"
                                      f"💥Добавить подписчиков💥 - 500 руб.\n"
                                      f"💥Анализ аудитории💥 - 100 руб.")

    @staticmethod
    def send_payment_details(update, context):
        user = update.message.from_user
        logging.info("User %s send payment details'", user.username)

        # For search chart_id:
        # https://api.telegram.org/bot1063238789:AAHkJaxZP316cNi-DK5caFOp_65b69YWHvI/getChat?chat_id=@traaabra
        context.bot.send_message(chat_id=-1001323553968,
                                 text=f"{user.username}")
        context.bot.send_photo(chat_id=-1001323553968, photo=update.message.photo[-1])

    @staticmethod
    def check_the_balance(update, context):
        user = update.message.from_user
        logging.info("User %s checked the balance'", user.username)
        context.bot.send_message(chat_id=update.message.chat_id,
                                 text=f"{user.username},\nВаш баланс - НИХУЯ!")

    @staticmethod
    def error(update, context):
        """Log Errors caused by Updates."""
        logging.warning('Update "%s" caused error "%s"', update, context.error)
