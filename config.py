import os
import logging.config
import yaml


def init_config():
    logger_file = 'config/logger_conf.yml'
    connect_file = 'config/connect_conf.yml'

    if not (os.path.exists(logger_file) or os.path.exists(connect_file)):
        logging.error("Config file or File folder not found")
    else:
        with open(logger_file, 'r') as stream_log, open(connect_file, 'r') as stream_connect:
            config_log = yaml.load(stream_log, Loader=yaml.FullLoader)
            config_connect = yaml.load(stream_connect, Loader=yaml.FullLoader)

        logging.config.dictConfig(config_log)

        return config_connect
